import React, { Component } from 'react';
import './App.css';
import { Provider } from 'react-redux';
import store from './store';
import Home from './components/Home/Home';
import Archive from './components/archive/archive';
import Navbar from './components/navbar/navbar';
import UrlToSelector from './components/urlToSelector/urlToSelector';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import Tasks from './components/Tasks/tasks';
import Results from './components/archive/results';
import Add from './components/urlToSelector/add';
import AddTask from './components/Tasks/add';
class App extends Component {
	render() {
		return (
			<Router>
				<div>
					<Navbar />
					<div className="contentGridContainer">
						<div className="content">
							<Provider store={store}>
								<Switch>
									<Route path="/home" render={() => <Home />} />
									<Route path="/tasks/add" render={(params) => <AddTask params={params} />} />
									<Route path="/tasks" render={() => <Tasks />} />
									<Route path="/archive" render={() => <Archive />} />

									<Route path="/urltoselector/add" render={(params) => <Add params={params} />} />
									<Route path="/urltoselector" render={() => <UrlToSelector path />} />

									<Route path="/results/:jobId" render={(params) => <Results params={params} />} />
									<Route exact path="/" render={() => <Redirect to="/home" />} />
								</Switch>
							</Provider>
						</div>
					</div>
				</div>
			</Router>
		);
	}
}

export default App;
