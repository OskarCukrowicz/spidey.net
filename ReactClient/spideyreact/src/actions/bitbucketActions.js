import { FETCH_ACTIVITY, FETCH_DEVELOPERS } from './types';
import { getDevelopers, getDevelopementActivity } from '../services/bitbucketService';
import axios from 'axios';

export const fetchDevelopers = () => (dispatch) => {
	var developersDataPromises = getDevelopers();

	var results = [];
	axios.all(developersDataPromises).then((results) => {
		results.forEach((data) => {
			results.push(data.data);
		});

		dispatch({
			type: FETCH_DEVELOPERS,
			payload: results.filter((x) => x.username != undefined) // i don't even know why it makes it work
		});
	});
};

export const fetcthDevelopementActivity = () => (dispatch) => {
	getDevelopementActivity().then((result) => {
		var commmits = result.data.values.filter((x) => x.author.user != null).slice(0, 5).map((value) => ({
			author: {
				display_name: value.author.user.display_name,
				avatar: value.author.user.links.avatar.href
			},
			message: value.message
		}));

		dispatch({
			type: FETCH_ACTIVITY,
			payload: commmits
		});
	});
};
