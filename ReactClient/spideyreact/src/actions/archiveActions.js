import { FETCH_ARCHIVE } from './types';
import axios from 'axios';
import config from '../config.json';
export const fetchArchive = () => (dispatch) => {
	axios.get(`${config.spideyApi}/archive/GetArchivedJobs`).then((result) => {
		dispatch({
			type: FETCH_ARCHIVE,
			payload: result.data.result
		});
	});
};
