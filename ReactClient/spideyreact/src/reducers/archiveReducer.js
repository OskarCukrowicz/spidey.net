import { FETCH_ARCHIVE } from '../actions/types';

const initialState = {
	items: []
};

export default function(state = initialState, action) {
	switch (action.type) {
		case FETCH_ARCHIVE: {
			return {
				...state,
				items: action.payload
			};
		}

		default: {
			return state;
		}
	}
}
