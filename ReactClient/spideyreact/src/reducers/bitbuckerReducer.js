import {FETCH_DEVELOPERS, FETCH_ACTIVITY} from '../actions/types';


const initialState = {
    developersData:[],
    developmentActivity:[]
};

export default function(state = initialState, action){
    switch(action.type)
    {
        case FETCH_DEVELOPERS:
        {
            return{
                ...state,
                developersData: action.payload
            };
        }

        case FETCH_ACTIVITY:
        {
            return{
                ...state,
                developmentActivity: action.payload
            }
        }
        
        default:
            return state;
    }
}