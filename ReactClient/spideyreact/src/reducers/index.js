import { combineReducers } from 'redux';
import taskReducer from './tasksReducer';
import archiveReducer from './archiveReducer';
import bitbucketReducer from './bitbuckerReducer';
export default combineReducers({
	tasks: taskReducer,
	bitbucket: bitbucketReducer,
	archive: archiveReducer
});
