import config from '../config.json';
import axios from 'axios';

export const getDevelopers = () => {
    var fetches = [];
    config.developers.forEach(developer => {
        fetches.push(axios.get(`${config.bitbucketApi}/users/${developer}`));
    })

    return fetches
}

export const getDevelopementActivity = () =>{
    return axios.get(`${config.bitbucketApi}/repositories/${config.repoData.owner}/${config.repoData.repoName}/commits/`);
}