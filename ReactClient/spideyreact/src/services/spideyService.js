import config from '../config.json';
import axios from 'axios';



export const getResults = (jobId, from, take) => {
    var result = axios.post(`${config.spideyApi}/Archive/GetJobResults`, {
        jobId: jobId,
        from: from,
        take: take
    });
    return result
}

export const removeArchive = (id) => {
    var result = axios.post(`${config.spideyApi}/Archive/Remove`, {
        id: id,
    });
    return result
}

export const getArchiveJobs = () => {
    var result = axios.get(`${config.spideyApi}/archive/GetArchivedJobs`);
    return result;
}

export const startJob = (keywords, seedUrl, searchDepth) => {
    debugger;
    var result = axios.post(`${config.spideyApi}/Jobs/Start`, {
        keywords: keywords,
        seedUrl: seedUrl,
        searchDepth: searchDepth
    });
    return result
}


export const stopJob = (jobId) => {
    var result = axios.post(`${config.spideyApi}/Jobs/Stop`, {
        jobId: jobId,
    });
    return result
}

export const listJobs = () => {
    var result = axios.get(`${config.spideyApi}/Jobs/List`);
    return result
}

export const addRegex = (url, selector) => {
    var result = axios.post(`${config.spideyApi}/RegexMatching/Add`, {
        selector: selector,
        regex: url
    });
    return result
}

export const removeRegex = (id) => {
    var result = axios.post(`${config.spideyApi}/RegexMatching/Remove`, {
        id: id
    });
    return result
}

export const listRegex = () => {
    var result = axios.get(`${config.spideyApi}/RegexMatching/GetAll`);
    return result
}
