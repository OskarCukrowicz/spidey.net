import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetcthDevelopementActivity } from '../../actions/bitbucketActions';

class DevelopmentActivity extends Component {
	componentWillMount() {
		this.props.fetcthDevelopementActivity();
	}

	render() {
		return (
			<div className="card w-50">
				<div className="card-header">
					<span>Recent commits</span>
				</div>

				<div className="card-body container">
					<div className="row">
						<h6 className="col d-flex justify-content-center">Author</h6>
						<h6 className="col d-flex justify-content-center">Message</h6>
						<hr className="w-100" />
					</div>
					{this.props.developmentActivity.map((activity, index) => (
						<div className="row" key={index}>
							<div className="col d-flex align-items-center">
								<img src={activity.author.avatar} className="developerAvatar mr-2" />
								<div>{activity.author.display_name}</div>
							</div>
							<div className="col d-flex align-items-center ">{activity.message}</div>
							<hr className="w-100" />
						</div>
					))}
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => ({
	developmentActivity: state.bitbucket.developmentActivity
});

export default connect(mapStateToProps, { fetcthDevelopementActivity })(DevelopmentActivity);
