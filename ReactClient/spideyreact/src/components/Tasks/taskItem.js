import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { stopJob } from '../../services/spideyService';
export default class taskItem extends Component {

    handleStop = () => {
        this.props.removeJob(this.props.item.jobId);
    }

    render() {
        return (
            <div className="card mt-2">
                <div className="card-header">
                    <div className="d-flex justify-content-between align-items-baseline">
                        <div className="d-flex">
                            <span className="mr-2 font-weight-bold">Keywords: </span>
                            <span>
                                {this.props.item.keywords
                                    .map((el, index) => <div key={index}>{el}</div>)}
                            </span>
                        </div>
                        <div>
                            <button className="btn btn-secondary btn-sm">
                                <i className="fas fa-plus" />
                            </button>
                        </div>
                    </div>
                </div>
                <div className="card-body">
                    <div className="d-flex flex-column">
                        <div className="d-flex">
                            <span className="mr-2">Seed url:</span>
                            <a target="_blank" href={this.props.item.seedUrl}>
                                Link
							</a>
                        </div>

                        <div className="d-flex flex-row-reverse">
                            <button onClick={this.handleStop} className="btn btn-danger btn-sm">Stop</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
