import React, { Component } from 'react';
import { WithContext as ReactTags } from 'react-tag-input';
import { listJobs, stopJob } from '../../services/spideyService';
import TaskItem from './taskItem';
import { Link } from 'react-router-dom';
export default class Tasks extends Component {
	componentDidMount() {
		this.getJobs();
	}


	getJobs = () => {
		listJobs().then(result => {
			this.setState({ jobs: result.data.result.jobs })
		})
	}

	removeJob = (jobId) => {
		var jobs = this.state.jobs.filter(x => x.jobId != jobId);
		stopJob(jobId);
		this.setState({ jobs: jobs })
	}

	render() {
		if (this.state != null) {
			return <div className="mt-2" >
				<div className="d-flex justify-content-end">
					<Link to="/tasks/add" className="btn btn-primary">Start new</Link>
				</div>

				{this.state.jobs.map(x => <TaskItem item={this.state.jobs[0]} removeJob={this.removeJob} />)}
			</div>;
		} else {
			return <div></div>
		}
	}
}
