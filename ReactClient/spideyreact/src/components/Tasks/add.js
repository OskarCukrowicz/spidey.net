import React, { Component } from 'react'
import { WithContext as ReactTags } from 'react-tag-input';
import { startJob } from '../../services/spideyService';
const delimiters = [','];
export default class AddTask extends Component {
    constructor(props) {
        super(props);
        this.state = {
            url: "",
            searchDepth: "",
            tags: []
        };
    }

    handleInputChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit = () => {
        if (this.state.url === "" || this.state.searchDepth <= 0 || this.state.tags.length === 0) {
            return;
        }


        startJob(this.state.tags.map(x => x.text), this.state.url, this.state.searchDepth).then(x => {
            this.props.params.history.push('tasks');
        })
    }

    handleAddition = (tag) => {
        this.setState(state => ({ tags: [...state.tags, tag] }));
    }
    handleDelete = (i) => {
        const { tags } = this.state;
        this.setState({
            tags: tags.filter((tag, index) => index !== i),
        });
    }
    render() {
        return (
            <div className="card mt-2">
                <div className="card-header">
                    <div>
                        Start new job
					</div>
                </div>
                <div className="card-body" >
                    <div className="form-group d-flex w-100">
                        <div className="w-50">
                            <label>Seed url</label>
                            <input
                                type="text"
                                className="form-control"
                                name="url"
                                onChange={this.handleInputChange}
                                placeholder="www.google.com" />
                        </div>
                        <div className="ml-2">
                            <label>Search depth</label>
                            <input
                                type="number"
                                className="form-control"
                                name="searchDepth"
                                onChange={this.handleInputChange} />
                        </div>
                    </div>
                    <div>
                        <ReactTags
                            tags={this.state.tags}
                            handleAddition={this.handleAddition}
                            handleDelete={this.handleDelete}
                        />
                    </div>
                    <div className="d-flex justify-content-end">
                        <button
                            type="submit"
                            onClick={this.handleSubmit}
                            handleDelete={this.handleDelete}
                            delimiters={delimiters}
                            className="btn btn-primary">Start</button>
                    </div>
                </div>
            </div>
        )
    }
}
