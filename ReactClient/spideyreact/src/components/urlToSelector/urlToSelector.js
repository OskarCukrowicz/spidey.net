import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { listRegex, removeRegex } from '../../services/spideyService';
export default class UrlToSelector extends Component {

	componentDidMount() {
		listRegex().then(x => {
			this.setState({
				items: x.data.result
			})
		});
	}

	handleRemove = (id) => {
		removeRegex(id);
		this.setState({
			items: this.state.items.filter(x => x.id !== id)
		})
	}

	render() {
		if (this.state == null) {
			return <div></div>
		}

		return (
			<div className="card mt-2">
				<div className="card-header d-flex justify-content-between align-items-baseline">
					<div>
						Regular expression to DOM selector
					</div>
					<Link to="urltoselector/add" className="btn btn-primary">Add</Link>
				</div>
				<div className="card-body" >
					<table className="table">
						<thead>
							<tr>
								<th scope="col">Regular expression</th>
								<th scope="col">Selector</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>
							{this.state.items.map(x =>
								<tr>
									<td>{x.regex}</td>
									<td>{x.selector}</td>
									<td>
										<button className="btn btn-danger" onClick={() => this.handleRemove(x.id)}>
											Remove
										</button>
									</td>
								</tr>)}
						</tbody>
					</table>
				</div>
			</div>
		);
	}
}
