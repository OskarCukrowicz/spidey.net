import React, { Component } from 'react'
import { addRegex } from '../../services/spideyService';
export default class Add extends Component {

    constructor(props) {
        super(props);
        this.state = {
            url: "",
            selector: ""
        };
    }

    handleInputChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubtmit = () => {
        if (this.state.selector === "" || this.state.url === "") {
            return;
        }


        addRegex(this.state.url, this.state.selector);
        this.props.params.history.push('/urltoselector');
    }

    render() {
        return (
            <div className="card mt-2">
                <div className="card-header">
                    <div>
                        Add new pair
					</div>
                </div>
                <div className="card-body" >
                    <div className="form-group">
                        <label>Regex for url matching</label>
                        <input
                            type="text"
                            className="form-control"
                            name="url"
                            onChange={this.handleInputChange}
                            placeholder="www.google.com/*" />
                    </div>
                    <div className="form-group">
                        <label>Selector</label>
                        <input
                            type="text"
                            className="form-control"
                            name="selector"
                            onChange={this.handleInputChange}
                            placeholder=".body" />
                    </div>
                    <div className="d-flex justify-content-end">
                        <button
                            type="submit"
                            onClick={this.handleSubtmit}
                            className="btn btn-primary">Add</button>
                    </div>
                </div>
            </div>
        )
    }
}
