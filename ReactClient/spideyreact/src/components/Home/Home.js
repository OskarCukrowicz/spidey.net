import React, { Component } from 'react'
import Developers from '../developers/developers';
import DevelopmentActivity from '../developmentActivity/developmentActivity';
import PullRequests from '../pullRequests/pullRequests';

class Home extends Component {


  render() {
    return (
      <div>
        <div className="d-flex mt-3">
          <div className="card w-50 mr-5">
            <div className="card-header">
              <span>Project Description</span>
            </div>

            <div className="card-body">
              <div>
                <p>
                  Spidey.Net is a web crawler developed by four students of
                  Poznan University of Technology.
              </p>
              </div>
            </div>
          </div>
          <Developers />

        </div>
        <div className="d-flex mt-5 align-items-start">
          <PullRequests />
          <DevelopmentActivity />
        </div>
      </div>
    )
  }
}

export default Home;
