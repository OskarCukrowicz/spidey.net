import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchDevelopers } from '../../actions/bitbucketActions';
import './developers.css';

class Developers extends Component {
	componentDidMount() {
		this.props.fetchDevelopers();
	}

	render() {
		return (
			<div className="card w-50">
				<div className="card-header">
					<span>Developers</span>
				</div>
				<div className="card-body">
					{this.props.developers.map((item, index) => (
						<div key={index}>
							<div className="d-flex align-items-center">
								<img src={item.links.avatar.href} className="developerAvatar mr-2" />
								<div>{item.display_name}</div>
							</div>
							<hr />
						</div>
					))}
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => ({
	developers: state.bitbucket.developersData
});

export default connect(mapStateToProps, { fetchDevelopers })(Developers);
