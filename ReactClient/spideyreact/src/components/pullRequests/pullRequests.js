import React, { Component } from 'react'

export default class PullRequests extends Component {
    render() {
        return (
            <div className="w-50 mr-5 d-flex">
                <div className="card w-50 mr-5">
                    <div className="card-body">
                        <h1 className="text-center">0</h1>
                        <p className="text-center text-muted">Open <br /> Pull requests</p>
                    </div>
                </div>

                <div className="card w-50">
                    <div className="card-body">
                        <h1 className="text-center">15</h1>
                        <p className="text-center text-muted"> Merged <br />Pull requests</p>
                    </div>
                </div>
            </div>
        )
    }
}
