import React, { Component } from 'react'

import { getResults } from '../../services/spideyService';

export default class Results extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {

        this.navigate(0)
    }


    navigate = (from) => {
        var jobId = this.props.params.match.params.jobId;
        getResults(jobId, from, 10).then(result => {
            this.setState({
                data: result.data.result,
                navigation: {
                    from: from,
                }
            });
        });
    }

    handleCLick = (from) => {
        if (from < 0 || !this.state.data.moreRecordsExist) {
            return;
        }
        this.navigate(from);
    }

    navigationBtns = () => {
        return <div className="d-flex justify-content-end">
            <button
                className={`btn btn-primary mr-1 ${this.state.navigation.from === 0 ? "disabled" : ""}`}
                onClick={() => { this.handleCLick(this.state.navigation.from - 30) }}
            >Previous</button>
            <button
                className={`btn btn-primary mr-1 ${this.state.data.moreRecordsExist ? "" : "disabled"}`}
                onClick={() => { this.handleCLick(this.state.navigation.from + 30) }}
            >Next</button>
        </div>
    }

    render() {
        if (this.state != null) {
            return (
                <div className="mt-3">
                    <h5>Crawl Results</h5>
                    {this.state.data.urls.map((el, index) =>
                        <div className="d-flex ">
                            <div className="mr-1 font-weight-bold">{this.state.navigation.from + index + 1}.</div>
                            <a href={el}>{el}</a>
                        </div>)
                    }

                    {this.navigationBtns()}
                </div>
            )
        }
        else {
            return <h4>Loading...</h4>
        }

    }
}
