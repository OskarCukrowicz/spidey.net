import React, { Component } from 'react';
import { connect } from 'react-redux';
import { removeArchive, getArchiveJobs } from '../../services/spideyService';
import ArchiveItem from './archiveItem';
export default class Archive extends Component {
	componentDidMount() {
		getArchiveJobs().then(x => {
			this.setState({ items: x.data.result.items });
		})
	}

	handleRemove = (id) => {
		removeArchive(id);
		this.setState({ items: this.state.items.filter(x => x.jobId !== id) });
	}


	render() {
		if (this.state == null) {
			return <div />;
		} else {
			return (
				<div className="d-flex flex-column mt-3">
					<div className="d-flex justify-content-center">
						<h4>Archive searches</h4>
					</div>
					{this.state.items.map((el, index) =>
						<ArchiveItem key={index} item={el} remove={this.handleRemove} />
					)}
				</div>
			);
		}
	}
}
