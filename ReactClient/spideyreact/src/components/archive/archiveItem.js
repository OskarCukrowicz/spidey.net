import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class ArchiveItem extends Component {


	render() {
		return (
			<div className="card mt-4">
				<div className="card-header">
					<div className="d-flex justify-content-between align-items-baseline">
						<div className="d-flex">
							<span className="mr-2 font-weight-bold">Keywords: </span>
							<span>
								{this.props.item.keywords
									.map((el, index) => <div key={index}>{el}</div>)}
							</span>
						</div>
						<div>
							<button className="btn btn-danger btn-sm" onClick={() => this.props.remove(this.props.item.jobId)}>
								<i className="fas fa-times fa-lg"></i>
							</button>
						</div>
					</div>
				</div>
				<div className="card-body">
					<div className="d-flex flex-column">
						<div className="d-flex">
							<span className="mr-2">Seed url:</span>
							<a target="_blank" href={this.props.item.seedUrl}>
								Link
							</a>
						</div>
						<div className="d-flex">
							<span className="mr-2">Matched sites count: </span>
							<span>{this.props.item.matchedSitesCount}</span>
						</div>

						<div className="d-flex flex-row-reverse">
							<Link to={`/results/${this.props.item.jobId}`} className="btn btn-primary btn-sm">Results</Link>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
