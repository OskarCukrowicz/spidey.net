﻿using Microsoft.EntityFrameworkCore;
using SpideyWebApp.Models.Database;
using System;

namespace Builders
{
    public class ContextBuilder
    {
        public SpideyContext BuildClean()
        {
            var dbContextOptionsBuilder = new DbContextOptionsBuilder<SpideyContext>();

            dbContextOptionsBuilder
                .EnableSensitiveDataLogging()
                .UseInMemoryDatabase(Guid.NewGuid().ToString());

            var databaseContext =
                new SpideyContext(dbContextOptionsBuilder.Options);
            databaseContext.Database.EnsureCreated();

            return databaseContext;
        }
    }
}
