﻿using SpideyWebApp.Models.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace Builders
{
    public class UrlToSelectorBuilder : Builder<UrlToSelectorBuilder, UrlToSelector>
    {
        public UrlToSelectorBuilder(SpideyContext context) : base(context)
        {
        }

        public UrlToSelectorBuilder WithRegularExpression(string regularExpression)
        {
            State.RegularExpression = regularExpression;
            return this;
        }

        public UrlToSelectorBuilder WithSelector(string selector)
        {
            State.Selector = selector;
            return this;
        }
    }
}
