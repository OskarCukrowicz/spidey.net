﻿using Microsoft.EntityFrameworkCore;
using SpideyWebApp.Models.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace Builders
{
    public abstract class Builder<T, TItem>
        where T : Builder<T, TItem>
        where TItem : new()
    {
        protected readonly SpideyContext Context;
        protected TItem State;

        protected Builder()
        {
            Init();
        }

        protected Builder(SpideyContext context)
        {
            Context = context;
            Init();
        }

        public virtual TItem Build()
        {
            Context.SaveChanges();
            return State;
        }

        public TItem BuildAndSave()
        {
            Save();
            return State;
        }

        public T Save()
        {
            var entity = State as Entity;
            if (entity != null)
            {
                if (Context.Entry(entity).State != EntityState.Modified)
                {
                    Context.Add(entity);
                }
            }
            else
            {
                throw new BuilderSaveException($"State {State} has to be an Entity or IdentityUser.");
            }

            Context.SaveChanges();
            return (T)this;
        }

        public T With(Action<TItem> operation)
        {
            operation.Invoke(State);
            return (T)this;
        }

        private void Init()
        {
            State = new TItem();
        }

        public TItem Get()
        {
            return State;
        }

        public TItem As(TItem item)
        {
            return State = item;
        }

        public T For(Action<T> operation)
        {
            operation.Invoke((T)this);
            return (T)this;
        }

    }

    public class BuilderSaveException : Exception
    {
        public BuilderSaveException(string msg) : base(msg)
        {
        }
    }
}
