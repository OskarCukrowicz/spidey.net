﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using Respawn;
using SpideyWebApp;
using SpideyWebApp.Models.Database;
using SpideyWebApp.Models.DataSeeder;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
namespace IntegrationTests
{
    public class IntegrationTestBase : NoParallelExecutionMarker, IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        protected readonly CustomWebApplicationFactory<Startup> WebApplicationFactory;
        protected readonly IHostingEnvironment Env;
        protected HttpClient HttpClient;
        protected SpideyContext Context;
        protected DataSeeder DataSeeder;
        protected string ConnectionString;
        private static Checkpoint checkpoint;

        public IntegrationTestBase(CustomWebApplicationFactory<Startup> webApplicationFactory)
        {

            this.WebApplicationFactory = webApplicationFactory;
            this.HttpClient = this.WebApplicationFactory.CreateDefaultClient();
            Context =
                webApplicationFactory.Server.Host.Services.GetService(typeof(SpideyContext)) as
                    SpideyContext;
            ConnectionString = Context.Database.GetDbConnection().ConnectionString;

            Env = webApplicationFactory.Server.Host.Services.GetService(typeof(IHostingEnvironment)) as
                IHostingEnvironment;

            DataSeeder = webApplicationFactory.Server.Host.Services.GetService(typeof(DataSeeder)) as DataSeeder;

            checkpoint = new Checkpoint
            {
                SchemasToInclude = new[]
                {
                    "public"
                },
                TablesToIgnore = new[]
                {
                    "__EFMigrationsHistory"
                },
                DbAdapter = DbAdapter.Postgres
            };
        }

        public void Dispose()
        {
            using (var conn = new NpgsqlConnection(ConnectionString))
            {
                conn.OpenAsync().GetAwaiter().GetResult();

                checkpoint.Reset(conn).GetAwaiter().GetResult();
            }
        }
        protected async Task GivenSeededData()
        {
            await DataSeeder.SeedData();
        }
    }
}
