using System;
using System.Reflection;
using Xunit;

namespace IntegrationTests
{
    public class EnforceIntegrationTests : NoParallelExecutionMarker
    {
        [Fact]
        public void EnsureProjectContainsOnlyIntegrationTests()
        {
            var allTypes = Assembly.GetExecutingAssembly().GetTypes();

            foreach (var type in allTypes)
            {
                var methods = type.GetMethods();
                foreach (var method in methods)
                {
                    var isTestClass = method.GetCustomAttribute(typeof(FactAttribute)) != null;

                    if (isTestClass)
                    {
                        Assert.True(type.IsSubclassOf(typeof(NoParallelExecutionMarker)));
                    }
                }
            }
        }
    }
}
