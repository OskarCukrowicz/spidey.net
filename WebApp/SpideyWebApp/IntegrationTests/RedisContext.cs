﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IntegrationTests
{
    public static class RedisContext
    {

        private static readonly string _devRedisConnectionString = "localhost:6379,allowAdmin=true";
        public static IDatabase GetCleanRedis()
        {
            var redis = ConnectionMultiplexer.Connect(_devRedisConnectionString);
            var server = redis.GetServer(_devRedisConnectionString.Split(',').First());
            server.FlushAllDatabases();

            return redis.GetDatabase();
        }



    }
}
