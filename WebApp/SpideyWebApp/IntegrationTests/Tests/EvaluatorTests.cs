﻿using Microsoft.Extensions.Configuration;
using Moq;
using SpideyWebApp.Infrastructure.Evaluator;
using SpideyWebApp.Infrastructure.HttpService;
using SpideyWebApp.Infrastructure.Parser;
using SpideyWebApp.Infrastructure.Redis;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace IntegrationTests.Tests
{
    public class EvaluatorTests : NoParallelExecutionMarker
    {
        private readonly string _evaluatorEndpoint = "http://localhost:9875/evaluate";
        [Fact]
        public async Task ShouldFillRedis()
        {
            var redisKey = "testRedis";
            var redisDb = RedisContext.GetCleanRedis();
            var parserResult = new ParserResultDto()
            {
                Url = "http://spidey.net",
                TextInHeaders = "Car",
                Metadata = "car",
                ParsedBody = "Car Car car"
            };
            var keywords = new List<string> { "car" };
            var evaluator = CreateEvaluator();

            await evaluator.Evaluate(parserResult, keywords, redisKey);

            string value = redisDb.StringGet(redisKey);

            Assert.NotNull(value);
        }


        private Evaluator CreateEvaluator()
        {
            var configuration = new Mock<IConfiguration>();
            configuration.SetupGet(x => x[It.IsAny<string>()]).Returns(_evaluatorEndpoint);

            var httpClientFactory = new Mock<IHttpClientFactory>();
            httpClientFactory.Setup(x => x.CreateClient(It.IsAny<string>())).Returns(new HttpClient());
            var httpService = new HttpService(httpClientFactory.Object);

            return new Evaluator(configuration.Object, httpService);
        }
    }
}
