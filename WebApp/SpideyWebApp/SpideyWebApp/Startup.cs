﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SpideyWebApp.Models.Database;
using MediatR;
using SpideyWebApp.Features.Hubs.Notification;
using SpideyWebApp.Infrastructure.Redis;
using SpideyWebApp.Infrastructure.Parser;
using SpideyWebApp.Infrastructure.Crawler;
using SpideyWebApp.Infrastructure.HttpService;
using SpideyWebApp.Infrastructure.RegexMatcher;
using Hangfire;
using Hangfire.PostgreSql;
using SpideyWebApp.Infrastructure.Evaluator;
using System.Text;
using SpideyWebApp.Infrastructure.RedisToPostgres;

namespace SpideyWebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddMvc()
                .AddJsonOptions(x => x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore)
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddSignalR();
            
            RegisterDependencyInjections(services);

            RunSeeders(services).Wait();
        }

        public void RegisterDependencyInjections(IServiceCollection services)
        {
            string connectionString = Configuration["ConnectionStrings:DefaultConnection"];
            services.AddHangfire(x => x.UsePostgreSqlStorage(connectionString));
            JobStorage.Current = new PostgreSqlStorage(connectionString);
            services.AddEntityFrameworkNpgsql()
                .AddDbContext<SpideyContext>(options =>
                {
                    options.EnableSensitiveDataLogging();
                    options.UseNpgsql(connectionString, x => x.MigrationsAssembly("SpideyWebApp"))
                        .ConfigureWarnings(warnings => warnings.Log(RelationalEventId.QueryClientEvaluationWarning));
                });
            services.AddHttpClient();
            services.AddMediatR(typeof(Startup));
            RegisterServices(services);
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCors(builder =>
            builder
                .AllowAnyOrigin()//allow cross origin resource sharing with react app
                .AllowAnyHeader()
                .AllowAnyMethod());

            app.UseSignalR(routes =>
            {
                routes.MapHub<NotificationHub>("/notification");
            });

            app.UseHttpsRedirection();
            app.UseHangfireServer();
            app.UseHangfireDashboard("/hangfire", new DashboardOptions());
            app.UseMvc();
        }

        private void RegisterServices(IServiceCollection services)
        {
            services.AddTransient<ICrawler, Crawler>();
            services.AddTransient<IParser, Parser>();
            services.AddTransient<ICrawler, Crawler>();
            services.AddTransient<IHttpService, HttpService>();
            services.AddTransient<IRegexMatcher, RegexMatcher>();
            services.AddSingleton<IRedisContext, RedisContext>();
            services.AddTransient<IRedisService, RedisService>();
            services.AddTransient<IEvaluator, Evaluator>();
            services.AddTransient<IRedisToPostgresService, RedisToPostgresService>();
        }
        private async Task RunSeeders(IServiceCollection services)
        {
            using (var serviceProvider = services.BuildServiceProvider())
            {
                var context = serviceProvider.GetService<SpideyContext>();
                await new Models.DataSeeder.DataSeeder(context).SeedData();
            }
        }
    }
}
