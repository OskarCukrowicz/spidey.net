﻿using MediatR;

namespace SpideyWebApp.Features
{
    public class BaseApiRequest<T> : IRequest<ApiResult<T>>
        where T :class
    {
    }
}
