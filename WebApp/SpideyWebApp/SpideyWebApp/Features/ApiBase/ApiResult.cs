﻿namespace SpideyWebApp.Features
{
    public class ApiResult<T>
        where T : class
    {
        public bool Succeeded { get; set; }
        public string ErrorMessage { get; set; }
        public T Result { get; set; }

        public ApiResult()
        {
            ErrorMessage = "";
            Succeeded = true;
        }

        public ApiResult(T result) : this()
        {
            Result = result;
        }
    }
}
