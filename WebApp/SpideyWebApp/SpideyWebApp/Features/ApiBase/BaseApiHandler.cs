﻿using FluentValidation;
using MediatR;
using SpideyWebApp.Models.Database;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SpideyWebApp.Features
{
    public abstract class BaseApiHandler<TRequest, TResponse> : IRequestHandler<TRequest, ApiResult<TResponse>>
        where TRequest : IRequest<ApiResult<TResponse>>
        where TResponse : class, new()

    {
        protected SpideyContext Context { get; set; }
        public BaseApiHandler(SpideyContext context)
        {
            Context = context;
        }

        public abstract Task<TResponse> Execute(TRequest request, CancellationToken token);

        public async Task<ApiResult<TResponse>> Handle(TRequest request, CancellationToken cancellationToken)
        {
            try
            {
                return new ApiResult<TResponse>(await Execute(request, cancellationToken));
            }
            catch (Exception exception)
            {
                return new ApiResult<TResponse>()
                {
                    Succeeded = false,
                    ErrorMessage = exception?.Message ?? "An unknown error occured"
                };
            }

        }
    }
}
