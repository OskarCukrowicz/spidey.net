﻿using Microsoft.EntityFrameworkCore;
using SpideyWebApp.Models.Database;
using SpideyWebApp.Models.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace SpideyWebApp.Features.RegexMatching.Edit
{
    public class Command : BaseApiRequest<Result>
    {
        public int Id { get; set; }
        public string Selector { get; set; }
        public string Regex { get; set; }
    }

    public class Result
    {

    }

    public class Handler : BaseApiHandler<Command, Result>
    {
        public Handler(SpideyContext context) : base(context)
        {
        }

        public override async Task<Result> Execute(Command request, CancellationToken token)
        {
            if(!new Validator().Validate(request).IsValid)
            {
                throw new EditRegexException("Provided values are not valid");
            }
            var urlToSelector = await new Repository<UrlToSelector>(Context).GetById(request.Id);

            urlToSelector.RegularExpression = request.Regex;
            urlToSelector.Selector = request.Selector;

            Context.UrlToSelector.Update(urlToSelector);
            Context.SaveChanges();

            return new Result();
        }
    }

}
