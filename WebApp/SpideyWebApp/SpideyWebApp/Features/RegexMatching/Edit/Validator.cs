﻿using FluentValidation;
using SpideyWebApp.Models.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Features.RegexMatching.Edit
{
    public class Validator : AbstractValidator<Command>
    {
        public Validator()
        {
            RuleFor(x => x.Id)
                .NotEqual(0);

            RuleFor(x => x.Regex)
                .NotEmpty()
                .Must(Validation.BeValidRegex);

            RuleFor(x => x.Selector)
                .NotEmpty();
        }
    }
}
