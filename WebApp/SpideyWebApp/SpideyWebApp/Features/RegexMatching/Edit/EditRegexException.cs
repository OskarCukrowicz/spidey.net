﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Features.RegexMatching.Edit
{
    public class EditRegexException : Exception
    {
        public EditRegexException(string message) : base(message)
        {
        }
    }
}
