﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace SpideyWebApp.Features.RegexMatching
{
    public class RegexMatchingController : BaseApiController
    {
        public RegexMatchingController(IMediator mediator) : base(mediator)
        {
        }

        [HttpPost]
        public async Task<IActionResult> Add([FromBody] Add.Command command, CancellationToken token)
        {
            var result = await Mediator.Send(command, token);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Edit([FromBody]Edit.Command command, CancellationToken token)
        {
            var result = await Mediator.Send(command, token);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Remove([FromBody]Remove.Command command, CancellationToken token)
        {
            var result = await Mediator.Send(command, token);
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery]GetAll.Query query, CancellationToken token)
        {
            var result = await Mediator.Send(query, token);
            return Ok(result);
        }
    }
}