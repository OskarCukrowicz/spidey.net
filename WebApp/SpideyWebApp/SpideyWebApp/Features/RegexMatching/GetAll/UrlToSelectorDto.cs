﻿using SpideyWebApp.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Features.RegexMatching.GetAll
{
    public class UrlToSelectorDto
    {
        public int Id { get; set; }
        public string Regex { get; set; }
        public string Selector { get; set; }

        public UrlToSelectorDto(UrlToSelector urlToSelector)
        {
            Id = urlToSelector.Id;
            Regex = urlToSelector.RegularExpression;
            Selector = urlToSelector.Selector;
        }
    }
}
