﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SpideyWebApp.Models.Database;

namespace SpideyWebApp.Features.RegexMatching.GetAll
{
    public class Query : BaseApiRequest<List<UrlToSelectorDto>>
    {
    }

    public class Handler : BaseApiHandler<Query, List<UrlToSelectorDto>>
    {
        public Handler(SpideyContext context) : base(context)
        {
        }

        public override async Task<List<UrlToSelectorDto>> Execute(Query request, CancellationToken token)
        {
            var result = await Context.UrlToSelector
                .Select(x => new UrlToSelectorDto(x))
                .ToListAsync(token);

            return result;
        }
    }
}
