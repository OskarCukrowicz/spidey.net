﻿using FluentValidation;
using SpideyWebApp.Models.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SpideyWebApp.Features.RegexMatching.Add
{
    public class Validator : AbstractValidator<Command>
    {
        public Validator()
        {
            RuleFor(x => x.Regex)
                .NotEmpty()
                .Must(Validation.BeValidRegex);

            RuleFor(x => x.Selector)
                .NotEmpty();
        }
    }
}
