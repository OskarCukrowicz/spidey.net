﻿using MediatR;
using SpideyWebApp.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace SpideyWebApp.Features.RegexMatching.Add
{
    public class Command : BaseApiRequest<Result>
    {
        public string Selector { get; set; }
        public string Regex { get; set; }
    }

    public class Result
    {
        public int Id { get; set; }
    }
    public class Handler : BaseApiHandler<Command, Result>
    {
        public Handler(SpideyContext context) : base(context)
        {
        }

        public override async Task<Result> Execute(Command request, CancellationToken token)
        {
            if(!new Validator().Validate(request).IsValid)
            {
                throw new AddRegexException("Provided values are not valid");
            }

            var urlToSelector = new UrlToSelector()
            {
                RegularExpression = request.Regex,
                Selector = request.Selector
            };

            Context.UrlToSelector.Add(urlToSelector);
            await Context.SaveChangesAsync(token);

            return new Result()
            {
                Id = urlToSelector.Id
            };
        }
    }
}
