﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Features.RegexMatching.Add
{
    public class AddRegexException : Exception
    {
        public AddRegexException(string message) : base(message)
        {
        }
    }
}
