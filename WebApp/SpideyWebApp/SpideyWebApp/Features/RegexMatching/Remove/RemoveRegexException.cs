﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Features.RegexMatching.Remove
{
    public class RemoveRegexException : Exception
    {
        public RemoveRegexException(string message) : base(message)
        {
        }
    }
}
