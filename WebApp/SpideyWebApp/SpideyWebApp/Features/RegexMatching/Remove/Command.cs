﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using SpideyWebApp.Features.Jobs.Start;
using SpideyWebApp.Models.Database;
using SpideyWebApp.Models.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SpideyWebApp.Features.RegexMatching.Remove
{
    public class Command : BaseApiRequest<Result>
    {
        public int Id { get; set; }
    }

    public class Handler : BaseApiHandler<Command, Result>
    {
        public Handler(SpideyContext context) : base(context)
        {
        }

        public override async Task<Result> Execute(Command request, CancellationToken token)
        {
            if(!new Validator().Validate(request).IsValid)
            {
                throw new RemoveRegexException("Provided values are not valid");
            }

            var regex = await new Repository<UrlToSelector>(Context).GetById(request.Id);;

            Context.UrlToSelector.Remove(regex);
            Context.SaveChanges();

            return new Result();
        }
    }
}
