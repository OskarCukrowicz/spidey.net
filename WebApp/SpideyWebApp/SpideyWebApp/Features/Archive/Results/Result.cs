﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Features.Archive.Results
{
    public class Result
    {
        public List<string> Urls { get; set; }
        public bool MoreRecordsExist { get; set; }
    }

}
