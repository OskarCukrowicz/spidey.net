﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SpideyWebApp.Models.Database;

namespace SpideyWebApp.Features.Archive.Results
{
    public class Query : BaseApiRequest<Result>
    {
        public int JobId { get; set; }
        public int From { get; set; }
        public int Take { get; set; }
    }

    public class Handler : BaseApiHandler<Query, Result>
    {
        public Handler(SpideyContext context) : base(context)
        {
        }

        public override async Task<Result> Execute(Query request, CancellationToken token)
        {
            var urls = Context.MatchedSites.Where(x => x.JobId == request.JobId)
                .OrderByDescending(x => x.Score)
                .Skip(request.From)
                .Take(request.Take)
                .Select(x => x.Url)
                .ToList();

            return new Result()
            {
                Urls = urls,
                MoreRecordsExist = request.Take == urls.Count
            };
        }
    }

}
