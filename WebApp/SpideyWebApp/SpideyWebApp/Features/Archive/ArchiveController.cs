﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace SpideyWebApp.Features.Archive
{
    public class ArchiveController : BaseApiController
    {

        public ArchiveController(IMediator mediator) : base(mediator)
        {
        }

        [HttpGet]
        public async Task<IActionResult> GetArchivedJobs()
        {
            var result = await Mediator.Send(new Jobs.Command());

            return new JsonResult(result);
        }

        [HttpPost]
        public async Task<IActionResult> GetJobResults([FromBody]Results.Query query, CancellationToken token)
        {
            var result = await Mediator.Send(query, token);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Remove([FromBody] Remove.Command command, CancellationToken token)
        {
            await Mediator.Send(command, token);

            return Ok();
        }
    }
}