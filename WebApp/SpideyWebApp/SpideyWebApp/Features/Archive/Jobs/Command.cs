﻿using Microsoft.EntityFrameworkCore;
using SpideyWebApp.Models.Database;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SpideyWebApp.Features.Archive.Jobs
{
    public class Command : BaseApiRequest<ArchiveJobListDto>
    {
    }

    public class Handler : BaseApiHandler<Command, ArchiveJobListDto>
    {
        public Handler(SpideyContext context) : base(context)
        {
        }

        public override async Task<ArchiveJobListDto> Execute(Command request, CancellationToken token)
        {
            var result = await Context.Jobs
                .Include(x => x.Keywords)
                .Include(x => x.MatchedSites)
                .Select(x => new ArchiveJobItemDto()
                {
                    Keywords = x.Keywords.Select(y => y.Word).ToList(),
                    SearchDate = x.StartDate,
                    SeedUrl = x.SeedUrl,
                    MatchedSitesCount = x.MatchedSites.Count,
                    JobId = x.Id
                })
                .ToListAsync();

            return new ArchiveJobListDto() { Items = result };
        }
    }
}
