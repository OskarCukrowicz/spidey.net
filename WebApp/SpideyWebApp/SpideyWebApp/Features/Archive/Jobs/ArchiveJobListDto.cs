﻿using System;
using System.Collections.Generic;

namespace SpideyWebApp.Features.Archive.Jobs
{
    public class ArchiveJobListDto
    {
        public List<ArchiveJobItemDto> Items { get; set; }
    }

    public class ArchiveJobItemDto
    {
        public int JobId { get; set; }
        public List<string> Keywords { get; set; }
        public string SeedUrl { get; set; }
        public DateTime SearchDate { get; set; }
        public int MatchedSitesCount { get; set; }
    }
}
