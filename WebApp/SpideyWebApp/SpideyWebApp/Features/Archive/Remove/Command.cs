﻿using Microsoft.EntityFrameworkCore;
using SpideyWebApp.Features.ApiBase;
using SpideyWebApp.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SpideyWebApp.Features.Archive.Remove
{
    public class Command : BaseApiRequest<EmptyResponse>
    {
        public int Id { get; set; }
    }

    public class Handler : BaseApiHandler<Command, EmptyResponse>
    {
        public Handler(SpideyContext context) : base(context)
        {
        }

        public override async Task<EmptyResponse> Execute(Command request, CancellationToken token)
        {
            var job = await Context.Jobs
                .Include(x => x.MatchedSites)
                .Include(x => x.Keywords)
                .Include(x => x.VisitedSited)
                .FirstOrDefaultAsync(x => x.Id == request.Id, token);

            Context.MatchedSites.RemoveRange(job.MatchedSites);
            Context.VisitedSites.RemoveRange(job.VisitedSited);
            Context.Keywords.RemoveRange(job.Keywords);
            Context.Jobs.Remove(job);
            await Context.SaveChangesAsync(token);

            return new EmptyResponse();
        }
    }

}
