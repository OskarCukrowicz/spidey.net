﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Hangfire;
using SpideyWebApp.Models.Database;

namespace SpideyWebApp.Features.Jobs.Stop
{
    public class Command : BaseApiRequest<Result>
    {
        public int JobId { get; set; }
    }

    public class Result
    {

    }
    public class Handler : BaseApiHandler<Command, Result>
    {
        public Handler(SpideyContext context) : base(context)
        {
        }

        public override async Task<Result> Execute(Command request, CancellationToken token)
        {
            var job = Context.Jobs
                .FirstOrDefault(x => x.Id == request.JobId);

            job.Status = JobStatus.Finished;
            BackgroundJob.Delete(job.JobId);

            Context.SaveChanges();

            return new Result();
        }
    }

}
