﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SpideyWebApp.Models.Database;

namespace SpideyWebApp.Features.Jobs.List
{
    public class Query : BaseApiRequest<Result>
    {
        
    }
    public class Result
    {
        public class Item
        {
            public List<string> Keywords { get; set; }
            public string SeedUrl { get; set; }
            public int JobId { get; set; }
        }
        public List<Item> Jobs { get; set; }
    }


    public class Handler : BaseApiHandler<Query, Result>
    {
        public Handler(SpideyContext context) : base(context)
        {
        }

        public override async Task<Result> Execute(Query request, CancellationToken token)
        {
            var items =  await Context.Jobs
                .Include(x => x.Keywords)
                .Where(x => x.Status == JobStatus.Running)
                .Select(x => new Result.Item
                {
                    JobId = x.Id,
                    Keywords = x.Keywords.Select(y => y.Word).ToList(),
                    SeedUrl = x.SeedUrl
                }).ToListAsync(token);

            return new Result()
            {
                Jobs = items
            };
        }
    }

}
