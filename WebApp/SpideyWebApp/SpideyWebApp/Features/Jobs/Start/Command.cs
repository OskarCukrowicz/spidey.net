﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Hangfire;
using SpideyWebApp.Infrastructure.Crawler;
using SpideyWebApp.Infrastructure.RedisToPostgres;
using SpideyWebApp.Models.Database;

namespace SpideyWebApp.Features.Jobs.Start
{
    public class Command : BaseApiRequest<Result>
    {
        public List<string> Keywords { get; set; }
        public string SeedUrl { get; set; }
        public int SearchDepth { get; set; }
    }

    public class Handler : BaseApiHandler<Command, Result>
    {
        public Handler(SpideyContext context) : base(context)
        {
        }

        public override async Task<Result> Execute(Command request, CancellationToken token)
        {
            var redisKey = Guid.NewGuid().ToString();
            var job = new Job()
            {
                StartDate = DateTime.UtcNow,
                SeedUrl = request.SeedUrl,
                Keywords = request.Keywords.Select(x => new KeyWord() { Word = x }).ToList(),
                Status = JobStatus.Running,
            };

            Context.Jobs.Add(job);
            Context.SaveChanges();

            var jobId = BackgroundJob.Schedule<ICrawler>(x => x.Crawl(request.Keywords, request.SeedUrl, redisKey, request.SearchDepth, job.Id), TimeSpan.FromSeconds(45));

            job.JobId = jobId;

            Context.Jobs.Update(job);
            Context.SaveChanges();

            return new Result()
            {
                JobId = jobId
            };
        }
    }
}
