﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Features.Jobs.Start
{
    public class Result
    {
        public string JobId { get; set; }
    }
}
