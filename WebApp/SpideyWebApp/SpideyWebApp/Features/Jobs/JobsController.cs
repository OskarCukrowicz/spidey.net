﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace SpideyWebApp.Features.Jobs
{
    public class JobsController : BaseApiController
    {
        public JobsController(IMediator mediator) : base(mediator)
        {
        }

        public async Task<IActionResult> Start([FromBody]Start.Command command, CancellationToken token)
        {
            var result = await Mediator.Send(command, token);
            return new JsonResult(result);
        }

        public async Task<IActionResult> Stop([FromBody]Stop.Command command, CancellationToken token)
        {
            var result = await Mediator.Send(command, token);

            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> List([FromQuery]List.Query query, CancellationToken token)
        {
            var result = await Mediator.Send(query, token);

            return Ok(result);
        }
    }
}