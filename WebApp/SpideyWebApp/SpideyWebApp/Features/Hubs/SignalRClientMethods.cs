﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Features.Hubs
{
    public static class SignalRClientMethods
    {
        /// <summary>
        /// Triggers notification in navbar notification bell
        /// </summary>
        public static readonly string Notification = "newNotification";
    }
}
