﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Features.Hubs
{
    public class EventHandlerBase<T>
        where T : Hub
    {
        protected IHubContext<T> Hub { get; set; }

        public EventHandlerBase(IHubContext<T> hub)
        {
            Hub = hub;
        }
    }
}
