﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Features.Hubs.Notification.SendNotification
{
    public class NotificationMessage
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public int JobId { get; set; }
    }
}
