﻿using MediatR;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SpideyWebApp.Features.Hubs.Notification.SendNotification
{
    public class NotificationEvent : IRequest<bool>
    {
        public NotificationMessage Message { get; set; }
    }

    public class Handler : EventHandlerBase<NotificationHub>,
        IRequestHandler<NotificationEvent, bool>
    {
        public Handler(IHubContext<NotificationHub> hub) : base(hub)
        {
        }

        public async Task<bool> Handle(NotificationEvent request, CancellationToken cancellationToken)
        {
            await Hub.Clients.All.SendAsync(SignalRClientMethods.Notification, request.Message);

            return true;
        }
    }
}
