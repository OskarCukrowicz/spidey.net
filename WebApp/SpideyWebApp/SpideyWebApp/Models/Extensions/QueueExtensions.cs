﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Models.Extensions
{
    public static class QueueExtensions
    {
        public static Queue<T> EnqueueRange<T>(this Queue<T> queue, IEnumerable<T> items)
        {
            foreach (var item in items)
            {
                queue.Enqueue(item);
            }

            return queue;
        }

        public static IEnumerable<T> DequeueRange<T>(this Queue<T> queue, int howMany)
        {
            for (int i = 0; i < howMany && queue.Count > 0; i++)
            {
                yield return queue.Dequeue();
            }
        }


    }
}
