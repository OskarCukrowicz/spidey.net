﻿using SpideyWebApp.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Models.DataSeeder
{
    public abstract class DataSeederCore
    {
        protected SpideyContext Context { get; set; }

        public DataSeederCore(SpideyContext context)
        {
            Context = context;
        }

        public abstract bool DataExists();
        public abstract Task SeedData();
    }
}
