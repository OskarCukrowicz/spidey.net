﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SpideyWebApp.Models.Database;

namespace SpideyWebApp.Models.DataSeeder.Seeders
{
    public class FakeDataSeeder : DataSeederCore
    {
        public FakeDataSeeder(SpideyContext context) : base(context)
        {
        }

        public override bool DataExists()
        {
            return Context.Jobs.Any();
        }

        public override Task SeedData()
        {
            var matchedSites = new List<MatchedSite>
            {
                new MatchedSite()
                {
                    Title = "Introduction to ASP.NET Core | Microsoft Docs",
                    Description="Get an introduction to ASP.NET Core, a cross-platform, high-performance, open-source framework for building modern, cloud-based, ...",
                    Url = "https://docs.microsoft.com/en-us/aspnet/core/"
                },
                new MatchedSite()
                {
                    Title = "What is ASP.NET Core? | .NET",
                    Description = "ASP.NET Core is the open-source and cross-platform version of ASP.NET, a popular web-development framework for building web apps on the .NET platform.",
                    Url = "https://dotnet.microsoft.com/learn/web/what-is-aspnet-core"
                },
                new MatchedSite()
                {
                    Title= "GitHub - aspnet/AspNetCore: ASP.NET Core is a cross-platform .NET ...",
                    Description = "SP.NET Core is a cross-platform .NET framework for building modern cloud-based web applications on Windows, Mac, or Linux. - aspnet/AspNetCore.",
                    Url = "https://github.com/aspnet/AspNetCore"
                },
                new MatchedSite()
                {
                    Title ="ASP.NET Core - Wikipedia",
                    Description ="ASP.NET Core is a free and open-source web framework, and higher performance than ASP.NET, developed by Microsoft and the community. It is a modular ...",
                    Url ="https://en.wikipedia.org/wiki/ASP.NET_Core"
                },
                new MatchedSite()
                {
                    Title="ASP.NET Core Tutorial",
                    Description="ASP.NET Core Tutorial for Beginners - Learn ASP.NET Core in simple and easy steps starting from basic to advanced concepts with examples including ...",
                    Url="https://www.tutorialspoint.com/asp.net_core/"
                }
            };

            var visitedSites = new List<VisitedSite>();
            Enumerable.Range(0, 100).ToList().ForEach(x =>
            {
                visitedSites.Add(new VisitedSite()
                {
                    Url = "Doesnt.Matter.com",
                    HashedUrl = "Doesnt.Matter.Either.com"
                });
            });

            Context.Jobs.Add(new Job()
            {
                StartDate = DateTime.UtcNow.Subtract(TimeSpan.FromDays(2)),
                EndDate = DateTime.UtcNow,
                JobId = "123",
                SeedUrl = "https://www.google.com/search?rlz=1C1CHBF_enPL815PL815&ei=bGmBXJvmKevyqwG_zaPYBg&q=Asp.net+Core&oq=Asp.net+Core&gs_l=psy-ab.3..35i39l2j0i7i30j0j0i203j0j0i20i263j0i203l3.985.985..1441...0.0..0.85.85.1......0....1..gws-wiz.......0i71.8M5GAtIRsYY",
                Keywords = new List<KeyWord>() { new KeyWord() { Word = "Asp.net core" } },
                Status = JobStatus.Finished,
                MatchedSites = matchedSites,
                VisitedSited = visitedSites
            });

            Context.SaveChanges();

            return Task.CompletedTask;
        }
    }
}
