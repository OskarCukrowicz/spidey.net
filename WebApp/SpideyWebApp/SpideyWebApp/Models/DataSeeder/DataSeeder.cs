﻿using SpideyWebApp.Models.Database;
using SpideyWebApp.Models.DataSeeder.Seeders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Models.DataSeeder
{
    public class DataSeeder
    {
        private List<DataSeederCore> _seeders { get; set; }

        public DataSeeder(SpideyContext context)
        {
            _seeders = new List<DataSeederCore>
            {
                new FakeDataSeeder(context)
            };
        }

        public async Task SeedData()
        {
            foreach(var seeder in _seeders)
            {
                if(!seeder.DataExists())
                {
                    await seeder.SeedData();
                }
            }
        }
    }
}
