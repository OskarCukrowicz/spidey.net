﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Models.Repositories.Exceptions
{
    public class GetByIdException : Exception
    {
        public GetByIdException(Type t, int id) : base($"Entry with type {t} and id {id} was not found in database")
        {           
        }
    }
}
