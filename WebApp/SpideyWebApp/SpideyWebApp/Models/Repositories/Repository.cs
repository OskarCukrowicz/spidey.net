﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using SpideyWebApp.Models.Database;
using SpideyWebApp.Models.Repositories.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Models.Repositories
{
    public class Repository<T>
        where T : Entity
    {
        protected SpideyContext Context { get; set;}

        public Repository(SpideyContext context)
        {
            Context = context;
        }

        public async Task<T> GetById(int id, Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null)
        {
            IQueryable<T> query = Context.Set<T>();
            if (include != null)
            {
                query = include(query);
            }

            var entity = await query.FirstOrDefaultAsync(x => x.Id == id);
            if (entity is null)
            {
                throw new GetByIdException(typeof(T), id);
            }

            return entity;
        }
    }
}
