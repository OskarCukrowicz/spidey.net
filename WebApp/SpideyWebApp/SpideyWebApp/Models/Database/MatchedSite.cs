﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Models.Database
{
    public class MatchedSite : Entity
    {
        public string Url { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public double Score { get; set; }
        public Job Job { get; set; }
        public int JobId { get; set; }
    }
}
