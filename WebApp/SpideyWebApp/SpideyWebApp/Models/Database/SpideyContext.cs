﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Models.Database
{
    public class SpideyContext : DbContext
    {
        public SpideyContext(DbContextOptions<SpideyContext> options) : base(options)
        {
            if (!Database.IsInMemory())
            {
                Database.Migrate();
            }
        }

        public DbSet<Job> Jobs { get; set; }
        public DbSet<MatchedSite> MatchedSites { get; set; }
        public DbSet<VisitedSite> VisitedSites { get; set; }
        public DbSet<KeyWord> Keywords { get; set; }
        public DbSet<UrlToSelector> UrlToSelector { get; set; }
    }
}
