﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Models.Database
{
    public class VisitedSite : Entity
    {
        public string Url { get; set; }
        public string HashedUrl { get; set; }
    }
}
