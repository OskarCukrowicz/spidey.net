﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Models.Database
{
    public class Job : Entity
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string JobId { get; set; }
        public JobStatus Status { get; set; }
        public string SeedUrl { get; set; }
        public List<KeyWord> Keywords { get; set; }
        public List<MatchedSite> MatchedSites { get; set; }
        public List<VisitedSite> VisitedSited { get; set; }
    }

    public enum JobStatus
    {
        Running = 0,
        Stopped = 1,
        Finished = 2
    }
}
