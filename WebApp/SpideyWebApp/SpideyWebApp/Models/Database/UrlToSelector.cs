﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Models.Database
{
    public class UrlToSelector : Entity
    {
        public string Selector { get; set; }
        public string RegularExpression { get; set; }
    }
}
