﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Models.Database
{
    public class KeyWord : Entity
    {
        public string Word { get; set; }
    }
}
