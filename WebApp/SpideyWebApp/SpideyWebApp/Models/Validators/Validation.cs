﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SpideyWebApp.Models.Validators
{
    public static class Validation
    {
        public static bool BeValidRegex(string regex)
        {
            try
            {
                new Regex(regex);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }
    }
}
