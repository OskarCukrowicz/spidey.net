﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SpideyWebApp.Migrations
{
    public partial class KeywordsRename : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_KeyWords_Jobs_JobId",
                table: "KeyWords");

            migrationBuilder.DropPrimaryKey(
                name: "PK_KeyWords",
                table: "KeyWords");

            migrationBuilder.RenameTable(
                name: "KeyWords",
                newName: "Keywords");

            migrationBuilder.RenameIndex(
                name: "IX_KeyWords_JobId",
                table: "Keywords",
                newName: "IX_Keywords_JobId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Keywords",
                table: "Keywords",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Keywords_Jobs_JobId",
                table: "Keywords",
                column: "JobId",
                principalTable: "Jobs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Keywords_Jobs_JobId",
                table: "Keywords");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Keywords",
                table: "Keywords");

            migrationBuilder.RenameTable(
                name: "Keywords",
                newName: "KeyWords");

            migrationBuilder.RenameIndex(
                name: "IX_Keywords_JobId",
                table: "KeyWords",
                newName: "IX_KeyWords_JobId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_KeyWords",
                table: "KeyWords",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_KeyWords_Jobs_JobId",
                table: "KeyWords",
                column: "JobId",
                principalTable: "Jobs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
