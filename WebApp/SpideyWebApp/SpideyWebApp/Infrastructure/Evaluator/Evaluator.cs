﻿using Microsoft.Extensions.Configuration;
using SpideyWebApp.Infrastructure.Parser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Web;
using SpideyWebApp.Infrastructure.HttpService;

namespace SpideyWebApp.Infrastructure.Evaluator
{
    public class Evaluator : IEvaluator
    {
        private readonly string _evaluatorEndpoint;
        private IHttpService _httpService { get; set; }
        public Evaluator(IConfiguration configuration, IHttpService httpService)
        {
            _evaluatorEndpoint = $"{configuration["Evaluator:Endpoint"]}/evaluate";
            _httpService = httpService;
        }

        public async Task<double> Evaluate(ParserResultDto parsedHtml, List<string> keywords, string redisKey)
        {
            var dto = new EvaluatorDto(parsedHtml, keywords, redisKey);
            var result = await _httpService.PostAsJson(dto, _evaluatorEndpoint);

            try
            {
                return double.Parse(result, System.Globalization.CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                return 0.0;
            }
        }
    }
}
