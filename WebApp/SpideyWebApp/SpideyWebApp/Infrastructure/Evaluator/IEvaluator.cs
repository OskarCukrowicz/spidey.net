﻿using SpideyWebApp.Infrastructure.Parser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Infrastructure.Evaluator
{
    public interface IEvaluator
    {
        Task<double> Evaluate(ParserResultDto parsedHtml, List<string> keywords, string redisKey);
    }
}
