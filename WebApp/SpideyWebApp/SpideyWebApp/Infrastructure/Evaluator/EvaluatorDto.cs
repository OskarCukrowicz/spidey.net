﻿using SpideyWebApp.Infrastructure.Parser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace SpideyWebApp.Infrastructure.Evaluator
{
    public class EvaluatorDto
    {
        public string Body { get; set; }
        public string TextInHeaders { get; set; }
        public string Url { get; set; }
        public string Metadata { get; set; }
        public List<string> Keywords { get; set; }
        public string RedisKey { get; set; }

        public EvaluatorDto(ParserResultDto parsedHtml, List<string> keywords, string redisKey)
        {
            Body = parsedHtml.ParsedBody;
            TextInHeaders = parsedHtml.TextInHeaders;
            Url = HttpUtility.UrlEncode(parsedHtml.Url);
            Metadata = parsedHtml.Metadata;
            Keywords = keywords;
            RedisKey = redisKey;
        }
    }
}
