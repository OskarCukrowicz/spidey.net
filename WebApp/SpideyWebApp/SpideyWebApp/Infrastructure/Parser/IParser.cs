﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Infrastructure.Parser
{
    public interface IParser
    {
        List<string> GetUrls(string html);
        ParserExtractionResult Parse(string html, string url);
    }
}
