﻿using HtmlAgilityPack;
using SpideyWebApp.Infrastructure.RegexMatcher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace SpideyWebApp.Infrastructure.Parser
{
    public class Parser : IParser
    {
        private IRegexMatcher _regexMatcher { get; set; }
        public Parser(IRegexMatcher regexMatcher)
        {
            _regexMatcher = regexMatcher;
        }
        public List<string> GetUrls(string html)
        {
            try
            {
                var htmlDoc = new HtmlDocument();
                htmlDoc.LoadHtml(html);

                var htmlNodes = htmlDoc.DocumentNode.SelectNodes("//a");

                if (htmlNodes is null)
                {
                    return new List<string>();
                }

                var parsedUrls = new List<string>();
                foreach (var node in htmlNodes)
                {
                    if (AnchorContainsHref(node))
                    {
                        parsedUrls.Add(node.Attributes["href"].Value);
                    }
                }

                return parsedUrls;
            }
            catch(Exception)
            {
                return new List<string>();
            }
            
            
        }

        public ParserExtractionResult Parse(string html, string url)
        {
            try
            {
                var htmlDoc = new HtmlDocument();
                htmlDoc.LoadHtml(html);

                var mainNode = ApplySelector(htmlDoc, _regexMatcher.GetSelector(url));
                var metadataNode = ApplySelector(htmlDoc, "//meta");

                var result = ExtractDataFromMainNode(mainNode);
                result.Metadata = ExtractMetadata(metadataNode);
                return result;
            }
            catch(Exception)
            {
                return new ParserExtractionResult();
            }
                        
        }


        private bool AnchorContainsHref(HtmlNode node)
        {
            if (node.Attributes["href"] != null)
            {
                var href = node.Attributes["href"].Value ?? "";
                if (!String.IsNullOrEmpty(href) && !href.Contains("#"))
                {
                    return true;
                }
            }

            return false;
        }
        private string ExtractMetadata(HtmlNodeCollection metadata)
        {
            if(metadata is null)
            {
                return String.Empty;
            }

            var parsedMetadata = new StringBuilder();
            foreach (var node in metadata.Where(node => node.Attributes["name"]?.Value == "keywords"))
            {
                var keywords = node.GetAttributeValue("content", "");
                parsedMetadata.Append($"{keywords} ");
            }

            return NormalizeString(parsedMetadata.ToString());
        }

        private ParserExtractionResult ExtractDataFromMainNode(HtmlNodeCollection htmlBodyNodes)
        {
            var parsedBody = new StringBuilder();
            var parsedHeaders = new StringBuilder();
            var htmlHeaders = new Regex(@"h[1-6]", RegexOptions.Compiled);
            var htmlStyleOrScript = new Regex(@"(style|script)", RegexOptions.Compiled);
            foreach (var node in htmlBodyNodes.Descendants("#text"))
            {
                if (htmlHeaders.IsMatch(node.ParentNode.Name))
                {
                    parsedHeaders.Append($"{node.InnerText} ");
                }
                else if (!htmlStyleOrScript.IsMatch(node.ParentNode.Name))
                {
                    parsedBody.Append($"{node.InnerText} ");
                }
            }
           
            return new ParserExtractionResult()
            {
                ParsedBody = NormalizeString(parsedBody.ToString()),
                TextInHeaders = NormalizeString(parsedHeaders.ToString()),
            };        
        }

        private string NormalizeString(string toNormalize)
        {
            var regex = new Regex(@"\s+", RegexOptions.Compiled);
            return regex.Replace(toNormalize, " ").Trim();
        }
        private HtmlNodeCollection ApplySelector(HtmlDocument doc, string selector)
        {
            return doc.DocumentNode.SelectNodes(selector);          
        } 

    }
}
