﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Infrastructure.Parser
{
    public class ParserExtractionResult
    {
        public string TextInHeaders { get; set; }
        public string Metadata { get; set; }
        public string ParsedBody { get; set; }
    }
}
