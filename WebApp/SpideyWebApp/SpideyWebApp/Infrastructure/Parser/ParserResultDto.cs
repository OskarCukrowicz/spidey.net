﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Infrastructure.Parser
{
    public class ParserResultDto
    {
        public string Url { get; set; }
        public string TextInHeaders { get; set; }
        public string Metadata { get; set; }
        public string ParsedBody { get; set; }

        public ParserResultDto()
        {

        }

        public ParserResultDto(ParserExtractionResult extractionResult, string url)
        {
            TextInHeaders = extractionResult.TextInHeaders;
            Metadata = extractionResult.Metadata;
            ParsedBody = extractionResult.ParsedBody;
            Url = url;
        }
    }
}
