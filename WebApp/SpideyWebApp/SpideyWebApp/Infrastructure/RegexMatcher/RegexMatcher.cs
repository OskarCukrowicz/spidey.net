﻿using SpideyWebApp.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SpideyWebApp.Infrastructure.RegexMatcher
{
    public interface IRegexMatcher
    {
        string GetSelector(string url);
    }

    public class RegexMatcher : IRegexMatcher
    {
        private SpideyContext _context { get; set; }
        private readonly string _defaultSelector = "//body";
        public RegexMatcher(SpideyContext context)
        {
            _context = context;
        }
        public string GetSelector(string url)
        {
            //TODO: Cache them to avoid roundtrips to database
            var regularExpressionToSelecor = GetRegularExpressions();
            foreach (var pair in regularExpressionToSelecor)
            {
                var regex = new Regex(pair.RegularExpression);
                if (regex.IsMatch(url))
                {
                    return pair.Selector;
                }
            }

            return _defaultSelector;
        }

        private List<UrlToSelector> GetRegularExpressions()
        {
            return _context.UrlToSelector.ToList();
        }
    }
}
