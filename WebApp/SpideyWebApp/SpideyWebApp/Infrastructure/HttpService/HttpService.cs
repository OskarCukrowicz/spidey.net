﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SpideyWebApp.Infrastructure.HttpService
{
    public static class ContentType
    {
        public static string Json { get; set; }
    }

    public interface IHttpService
    {
        Task<string> GetUrl(string url);
        Task<Stream> GetUrlStream(string url);
        Task PostJson(string jsonContent, string url);
        Task<string> PostAsJson(object objectContent, string url);
    }

    public class HttpService : IHttpService
    {
        private HttpClient _client { get; set; }

        public HttpService(IHttpClientFactory factory)
        {
            _client = factory.CreateClient();
        }

        public async Task<string> GetUrl(string url)
        {
            using (var result = await _client.GetAsync(url))
            {
                using (var responseStream = new StreamReader(await result.Content.ReadAsStreamAsync(), Encoding.GetEncoding("iso-8859-1")))
                {
                    return responseStream.ReadToEnd();
                }
            }
        }

        public async Task<Stream> GetUrlStream(string url)
        {
            return await _client.GetStreamAsync(url);
        }

        public async Task PostJson(string jsonContent, string url)
        {
            await _client.PostAsJsonAsync(url, jsonContent);
        }

        public async Task<string> PostAsJson(object objectContent, string url)
        {
            using (var result = await _client.PostAsJsonAsync(url, objectContent))
            {
                using (var responseStream = new StreamReader(await result.Content.ReadAsStreamAsync()))
                {
                    return responseStream.ReadToEnd();
                }
            }
        }
    }
}
