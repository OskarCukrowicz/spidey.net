﻿using Newtonsoft.Json.Linq;
using SpideyWebApp.Infrastructure.Redis;
using SpideyWebApp.Models.Database;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Infrastructure.RedisToPostgres
{
    public interface IRedisToPostgresService
    {
        Task ToPostgreTransfer(string key, string jobId);
    }

    public class RedisToPostgresService : IRedisToPostgresService
    {
        const int howManyToPop = 1000;
        private IRedisService _redisService { get; set; }
        private SpideyContext _spideyContext { get; set; }
        public RedisToPostgresService(IRedisService redisService, SpideyContext spideyContext)
        {
            _redisService = redisService;
            _spideyContext = spideyContext;
        }


        public async Task ToPostgreTransfer(string key, string jobId)
        {
            var job = _spideyContext.Jobs.FirstOrDefault(x => x.JobId == jobId);
            RedisValue[] poppedValues = null;
            do
            {
                poppedValues = await _redisService.PopValueRangeAsync(key, howManyToPop);
                var pastSearches = new List<MatchedSite>();

                for (int i = 0; i < poppedValues.Length; i++)
                {
                    var valueJson = JObject.Parse(poppedValues[i]);
                    var url = (string)valueJson["Url"];
                    var score = (double)valueJson["Value"];
                    if (job != null)
                    {
                        pastSearches.Add(new MatchedSite() { Job = job, Url = url, Score = score });
                    }
                }
                await _spideyContext.MatchedSites.AddRangeAsync(pastSearches);
                await _spideyContext.SaveChangesAsync();
                pastSearches.Clear();

            } while (poppedValues.Length != 0);


            job.Status = JobStatus.Finished;
            _spideyContext.SaveChanges();
        }
    }
}
