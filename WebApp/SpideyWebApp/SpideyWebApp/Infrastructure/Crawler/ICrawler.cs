﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Infrastructure.Crawler
{
    public interface ICrawler
    {
        Task Crawl(List<string> keywords, string url, string crawlIdentifier, int searchDepth, int databaseJobId);
    }
}
