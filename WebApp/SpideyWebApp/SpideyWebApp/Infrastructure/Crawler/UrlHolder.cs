﻿using SpideyWebApp.Infrastructure.Parser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Infrastructure.Crawler
{
    public class UrlHolder
    {
        public Uri Url { get; set; }
        public Uri ParentUri { get; set; }
        public int Depth { get; set; }
        public string Html { get; set; }
        public List<string> Urls { get; set; }
        public ParserExtractionResult ExtractionResult { get; set; }

        public UrlHolder()
        {

        }

        public UrlHolder(string url, int depth)
        {
            Url = new Uri(url);
            Depth = depth;
        }
        public UrlHolder(string url, Uri parentUrl, int depth)
        {
            if (url.StartsWith("/"))
            {
                Url = new Uri($"{parentUrl.Scheme}://{parentUrl.Host}{url}");
            }
            else
            {
                Url = new Uri(url);
            }
            Depth = depth;
            ParentUri = parentUrl;
        }
    }
}
