﻿using SpideyWebApp.Infrastructure.Evaluator;
using SpideyWebApp.Infrastructure.HttpService;
using SpideyWebApp.Infrastructure.Parser;
using SpideyWebApp.Infrastructure.Redis;
using SpideyWebApp.Models.Database;
using SpideyWebApp.Models.Extensions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Infrastructure.Crawler
{
    public class Crawler : ICrawler
    {
        public IRedisService _redis { get; set; }
        public IEvaluator _evaluator { get; set; }
        public IHttpService _httpService { get; set; }
        public IParser _parser { get; set; }
        private SpideyContext _context { get; set; }
        private string _visitedUrlsKey { get; set; }

        public Crawler(IRedisService redis, IEvaluator evaluator, IHttpService httpService, IParser parser, SpideyContext context)
        {
            _parser = parser;
            _redis = redis;
            _httpService = httpService;
            _evaluator = evaluator;
            _visitedUrlsKey = Guid.NewGuid().ToString();
            _context = context;
        }

        public async Task Crawl(List<string> keywords, string seedUrl, string crawlIdentifier, int searchDepth, int databaseJobId)
        {
            var queue = new Queue<UrlHolder>();

            queue.Enqueue(new UrlHolder(seedUrl, 0));
            do
            {
                try
                {
                    var holder = queue.Dequeue();

                    await VisitUrl(holder);

                    holder.Urls = _parser.GetUrls(holder.Html).Distinct().ToList();
                    holder.ExtractionResult = _parser.Parse(holder.Html, holder.Url.ToString());

                    var unvisitedUrls = await _redis.GetUnvisitedUrls(holder.Urls, _visitedUrlsKey);
                    var newDepth = holder.Depth + 1;

                    if (newDepth < searchDepth)
                    {
                        queue.EnqueueRange(unvisitedUrls.Select(x => new UrlHolder(x, holder.Url, newDepth)));
                    }
                    var result = await _evaluator.Evaluate(new ParserResultDto(holder.ExtractionResult, holder.Url.ToString()), keywords, _visitedUrlsKey);

                    _context.MatchedSites.Add(new MatchedSite()
                    {
                        JobId = databaseJobId,
                        Score = result,
                        Url = holder.Url.ToString()
                    }); ;
                    _context.SaveChanges();
                }
                catch (Exception) { }//if something goes wrong just move on to next page
            } while (queue.Count != 0);
        }

        private async Task VisitUrl(UrlHolder holder)
        {
            await _redis.AddVisitedUrl(holder.Url.ToString(), _visitedUrlsKey);
            holder.Html = await _httpService.GetUrl(holder.Url.ToString());
        }

    }
}
