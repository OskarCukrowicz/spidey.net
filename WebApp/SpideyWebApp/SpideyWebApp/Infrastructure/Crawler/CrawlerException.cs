﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Infrastructure.Crawler
{
    public class CrawlerException : Exception
    {
        public CrawlerException(string message) : base(message)
        {
        }
    }
}
