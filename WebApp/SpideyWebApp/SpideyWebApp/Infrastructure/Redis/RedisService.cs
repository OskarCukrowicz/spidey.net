﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SpideyWebApp.Infrastructure.Redis
{
    public interface IRedisService
    {
        Task<bool> WasUrlVisited(string url, string key);
        Task<List<string>> GetUnvisitedUrls(List<string> urls, string key);
        Task AddVisitedUrl(string url, string key);
        Task AddVisitedUrlNoWait(string url, string key);
        Task AddVisitedUrls(List<string> urls, string key);
        Task<RedisValue[]> PopValueRangeAsync(string key, int howManyToPop);
    }

    public class RedisService : IRedisService
    {
        private IDatabase _redisDb { get; set; }
        public RedisService(IRedisContext context)
        {
            _redisDb = context.GetDatabase();
        }

        public async Task AddVisitedUrl(string url, string key)
        {
            await _redisDb.SetAddAsync(key, url);
        }

        public async Task<RedisValue[]> PopValueRangeAsync(string key, int howManyToPop)
        {
            return await _redisDb.SetPopAsync(key, howManyToPop);
        }

        public async Task AddVisitedUrls(List<string> urls, string key)
        {
            var operations = new List<Task>();
            foreach (var url in urls)
            {
                var operarion = _redisDb.SetAddAsync(key, url);
                operations.Add(operarion);
            }

            await Task.WhenAll(operations);
        }

        public async Task<List<string>> GetUnvisitedUrls(List<string> urls, string key)
        {
            var tasks = new List<Task<bool>>();
            foreach (var url in urls)
            {
                tasks.Add(_redisDb.SetContainsAsync(key, url));
            }
            await Task.WhenAll(tasks.ToArray());

            var unvisitedUrls = new List<string>();
            for (int i = 0; i < tasks.Count; i++)
            {
                if (tasks[i].Result is false)
                {
                    unvisitedUrls.Add(urls[i]);
                }
            }

            return unvisitedUrls;
        }

        public async Task<bool> WasUrlVisited(string url, string key)
        {
            return await _redisDb.SetContainsAsync(key, url);
        }

        public async Task AddVisitedUrlNoWait(string url, string key)
        {
            await _redisDb.SetAddAsync(key, url, CommandFlags.FireAndForget);
        }
    }
}
