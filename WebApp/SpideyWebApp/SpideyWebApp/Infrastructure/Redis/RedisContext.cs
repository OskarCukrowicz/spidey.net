﻿using Microsoft.Extensions.Configuration;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpideyWebApp.Infrastructure.Redis
{
    public interface IRedisContext
    {
        IDatabase GetDatabase();
    }
    public class RedisContext : IRedisContext
    {
        private ConnectionMultiplexer _redis { get; set; }

        public RedisContext(IConfiguration configuration)
        {
            _redis = ConnectionMultiplexer.Connect(configuration["Redis:ConnectionString"]);
        }

        public IDatabase GetDatabase()
        {
            return _redis.GetDatabase();
        }
    }
}
