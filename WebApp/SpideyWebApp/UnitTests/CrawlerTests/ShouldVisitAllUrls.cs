using Moq;
using SpideyWebApp.Infrastructure.Crawler;
using SpideyWebApp.Infrastructure.Evaluator;
using SpideyWebApp.Infrastructure.HttpService;
using SpideyWebApp.Infrastructure.Parser;
using SpideyWebApp.Infrastructure.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace UnitTests.CrawlerTests
{
    public class ShouldVisitAllUrls
    {
        private string _seedUrl = "urlOne";
        private List<string> _firstGenerationUrls = new List<string> { "urlTwo", "urlThree" };
        private List<string> _secondGenerationUrls = new List<string> { "urlFour", "urlFive", "urlSix", "urlSeven" };

        private List<string> visitedUrs = new List<string>();

        [Fact]
        public async Task ShouldVisitAllWebsites()
        {
            var redis = SetupRedisMock();
            var evaluator = SetupEvaluatorMock();
            var httpService = SetupHttpServiceMock();
            var parser = SetupParserMock();

            var crawler = new Crawler(redis.Object, evaluator.Object, httpService.Object, parser.Object);

            await crawler.Crawl(new List<string>(), "urlOne", "123", 5);



            var expectedVisits = _firstGenerationUrls.Concat(_secondGenerationUrls).Append(_seedUrl).OrderBy(x => x);
            Assert.Equal(expectedVisits, visitedUrs.OrderBy(x => x));
        }

        private Mock<IParser> SetupParserMock()
        {
            var mock = new Mock<IParser>();
            mock.Setup(x => x.GetUrls(_seedUrl))
                .Returns(_firstGenerationUrls);

            mock.Setup(x => x.GetUrls(_firstGenerationUrls.First()))
                .Returns(_secondGenerationUrls.Take(2).ToList());

            mock.Setup(x => x.GetUrls(_firstGenerationUrls.Last()))
                .Returns(_secondGenerationUrls.Skip(2).Take(2).ToList());


            mock.Setup(x => x.Parse(It.IsAny<string>(),It.IsAny<string>())).Returns(new ParserExtractionResult());
            mock.Setup(x => x.GetUrls(It.IsIn<string>(_secondGenerationUrls)))
                .Returns(new List<string>());

            return mock;
        }

        private Mock<IRedisService> SetupRedisMock()
        {
            var mock = new Mock<IRedisService>();

            mock.Setup(x => x.GetUnvisitedUrls(It.IsAny<List<string>>(), It.IsAny<string>()))
                .Returns<List<string>, string>((x, y) => Task.FromResult(x));

            mock.Setup(x => x.WasUrlVisited(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(false));

            return mock;
        }

        private Mock<IHttpService> SetupHttpServiceMock()
        {
            var mock = new Mock<IHttpService>();

            mock.Setup(x => x.GetUrl(It.IsAny<string>()))
                .Returns<string>(x =>
                {
                    visitedUrs.Add(x);
                    return Task.FromResult(x);
                });

            return mock;
        }

        private Mock<IEvaluator> SetupEvaluatorMock()
        {
            var mock = new Mock<IEvaluator>();

            mock.Setup(x => x.Evaluate(It.IsAny<ParserResultDto>(), It.IsAny<List<string>>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true));

            return mock;
        }
    }
}
