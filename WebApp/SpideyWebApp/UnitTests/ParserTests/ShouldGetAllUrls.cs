﻿using SpideyWebApp.Infrastructure.Parser;
using System;
using System.Collections.Generic;
using System.Text;
using UnitTests.ParserTests.MockHtmls;
using Xunit;
using System.Linq;
namespace UnitTests.ParserTests
{
    public class ShouldGetAllUrls
    {
        [Fact]
        public void ShouldGetAllUrlsFromHtml()
        {
            var parser = ParserBuilder.GetParser();
            var result = parser.GetUrls(MockHtml.Html1);

            Assert.Equal(7, result.Count);
            Assert.True(result.All(x => x == "http://www.test.com"));
        }
    }
}
