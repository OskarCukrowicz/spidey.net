﻿using Moq;
using SpideyWebApp.Infrastructure.Parser;
using SpideyWebApp.Infrastructure.RegexMatcher;
using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTests.ParserTests
{
    public static class ParserBuilder
    {
        public static Parser GetParser(string baseSelector = null)
        {
            var regexMatcher = new Mock<IRegexMatcher>();
            regexMatcher.Setup(x => x.GetSelector(It.IsAny<string>())).Returns(baseSelector ?? "//body");

            return new Parser(regexMatcher.Object);
        }
    }
}
