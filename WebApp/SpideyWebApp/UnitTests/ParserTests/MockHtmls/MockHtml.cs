﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTests.ParserTests.MockHtmls
{
    public static class MockHtml
    {
        public static string Html1 = @"<!DOCTYPE html>
		<html lang=""en"">
			<head>
				<title>c# - POSTing JsonObject With HttpClient From Web API - Stack Overflow</title>
				<link rel=""shortcut icon"" href=""https://cdn.sstatic.net/Sites/stackoverflow/img/favicon.ico?v=4f32ecc8f43d"">
				<link rel=""apple-touch-icon image_src"" href=""https://cdn.sstatic.net/Sites/stackoverflow/img/apple-touch-icon.png?v=c78bd457575a"">
				<link rel=""search"" type=""application/opensearchdescription+xml"" title=""Stack Overflow"" href=""/opensearch.xml"">
				<meta name=""viewport"" content=""width=device-width, height=device-height, initial-scale=1.0, minimum-scale=1.0"">
		 
		 
				<meta property=""og:type"" content= ""website"" />
				<meta property=""og:url"" content=""https://stackoverflow.com/questions/6117101/posting-jsonobject-with-httpclient-from-web-api""/>
				<meta property=""og:site_name"" content=""Stack Overflow"" />
				<meta property=""og:image"" itemprop=""image primaryImageOfPage"" content=""https://cdn.sstatic.net/Sites/stackoverflow/img/apple-touch-icon@2.png?v=73d79a89bded"" />
				<meta name=""twitter:card"" content=""summary""/>
				<meta name=""twitter:domain"" content=""stackoverflow.com""/>
				<meta name=""twitter:title"" property=""og:title"" itemprop=""name"" content=""POSTing JsonObject With HttpClient From Web API"" />
		        <script>js</script>
                <style>css</style>
			</head>
			<body>
					<div>
						<a href=""http://www.test.com"">Test</a>
						<a href=""http://www.test.com"">Test</a>
						<a href=""http://www.test.com"">Test</a>
						<a href=""http://www.test.com"">Test</a>
					</div>
		 
					<div>
							<a href=""http://www.test.com"">Test</a>
						<div>
								<a href=""http://www.test.com"">Test</a>
							<div>
								<div>
										<a href=""http://www.test.com"">Test</a>
								</div>
							</div>
						</div>
					</div>
                <script>js</script>
                <style>css</style>
			</body>
		</html>";

        public static string Html2 =
            @"<!DOCTYPE html>
            <html lang=""en"">
            <head></head>
            <body>
                <div>
                    <h1>header</h1>
                    

                    <ul class=""navbar-nav"">
                    <li class=""nav-item active"">
                    <p>Documentation</p>
                        <h1>header2</h1>
                    </li>
                     <li class=""nav-item "">
                <div>Knowledge  Base! </div>
            </li>
            </ul>

                </div>
                <div>
                    <div>
                        <h1>test</h1>
                        <p>test test</p>
                    </div>
                </div>
                <script>js</script>
                <style>css</style>
            </body>
            </html>";
        public static string Html3 = @"<!DOCTYPE html>
		<html lang=""en"">
			<head>
				<title>c# - POSTing JsonObject With HttpClient From Web API - Stack Overflow</title>
				<link rel=""shortcut icon"" href=""https://cdn.sstatic.net/Sites/stackoverflow/img/favicon.ico?v=4f32ecc8f43d"">
				<link rel=""apple-touch-icon image_src"" href=""https://cdn.sstatic.net/Sites/stackoverflow/img/apple-touch-icon.png?v=c78bd457575a"">
				<link rel=""search"" type=""application/opensearchdescription+xml"" title=""Stack Overflow"" href=""/opensearch.xml"">
				<meta name=""viewport"" content=""width=device-width, height=device-height, initial-scale=1.0, minimum-scale=1.0"">
		 		<meta name=""keywords"" content="".NET, DotNet, C#, VB.NET, F#, CSharp, VbNet, FSharp, SandBox, Visual Studio, Fiddle, Code Snippet"" />

		 
				<meta property=""og:type"" content= ""website"" />
				<meta property=""og:url"" content=""https://stackoverflow.com/questions/6117101/posting-jsonobject-with-httpclient-from-web-api""/>
				<meta property=""og:site_name"" content=""Stack Overflow"" />
				<meta property=""og:image"" itemprop=""image primaryImageOfPage"" content=""https://cdn.sstatic.net/Sites/stackoverflow/img/apple-touch-icon@2.png?v=73d79a89bded"" />
				<meta name=""twitter:card"" content=""summary""/>
				<meta name=""twitter:domain"" content=""stackoverflow.com""/>
				<meta name=""twitter:title"" property=""og:title"" itemprop=""name"" content=""POSTing JsonObject With HttpClient From Web API"" />
		 
			</head>
			<body>
					<div>
						<a href=""http://www.test1.com"">Test</a>
						<a href=""http://www.test2.com"">Test</a>
						<a href=""http://www.test3.com"">Test</a>
						<a href=""http://www.test4.com"">Test</a>
					</div>
		 
					<div>
							<a href=""http://www.test5.com"">Test</a>
						<div>
								<a href=""http://www.test6.com"">Test</a>
							<div>
								<div>
										<a href=""http://www.test7.com/123"">Test</a>
								</div>
							</div>
						</div>
					</div>
                <script>js</script>
                <style>css</style>
			</body>
		</html>";
    }
}
