﻿using SpideyWebApp.Infrastructure.Parser;
using System;
using System.Collections.Generic;
using System.Text;
using UnitTests.ParserTests.MockHtmls;
using Xunit;

namespace UnitTests.ParserTests
{
    public class ShouldGetParsedText
    {
        [Fact]
        public void ShouldGetParsedTextFromHtml()
        {
            var parser = ParserBuilder.GetParser();
            string result = parser.Parse(MockHtml.Html2, String.Empty).ParsedBody;

            string expectesResult = "Documentation Knowledge Base! test test";
            Assert.Equal(expectesResult, result);
        }
    }
}
