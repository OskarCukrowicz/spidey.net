﻿using SpideyWebApp.Infrastructure.Parser;
using System;
using System.Collections.Generic;
using System.Text;
using UnitTests.ParserTests.MockHtmls;
using Xunit;

namespace UnitTests.ParserTests
{
    public class ShouldGetAllMetadata
    {
        [Fact]
        public void ShouldGetAllMetadataFromHtml()
        {
            var parser = ParserBuilder.GetParser();
            string result = parser.Parse(MockHtml.Html3, String.Empty).Metadata;
            string result2 = parser.Parse(MockHtml.Html2, String.Empty).Metadata;
            string emptyMeta = "";

            string expectesResult = ".NET, DotNet, C#, VB.NET, F#, CSharp, VbNet, FSharp, SandBox, Visual Studio, Fiddle, Code Snippet";
            Assert.Equal(expectesResult, result);
            Assert.Equal(emptyMeta, result2);

        }
    }
}
