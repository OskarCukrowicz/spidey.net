﻿using SpideyWebApp.Infrastructure.Parser;
using System;
using System.Collections.Generic;
using System.Text;
using UnitTests.ParserTests.MockHtmls;
using Xunit;

namespace UnitTests.ParserTests
{
    public class ShouldGetParsedHeaders
    {
        [Fact]
        public void ShouldGetParsedHeadersFromHtml()
        {
            var parser = ParserBuilder.GetParser();
            string result = parser.Parse(MockHtml.Html2, String.Empty).TextInHeaders;

            string expectesResult = "header header2 test";
            Assert.Equal(expectesResult, result);
        }
    }
}
