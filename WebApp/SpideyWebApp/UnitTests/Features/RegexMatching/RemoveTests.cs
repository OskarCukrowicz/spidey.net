﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Builders;
using SpideyWebApp.Features.RegexMatching.Remove;
using Xunit;
namespace UnitTests.Features.RegexMatching
{
    public class RemoveTests
    {
        [Fact]
        public async Task ShouldEdit()
        {
            var context = new ContextBuilder().BuildClean();

            var urlToSelector = new UrlToSelectorBuilder(context)
                .WithRegularExpression("")
                .WithSelector("").BuildAndSave();

            var command = new Command()
            {
                Id = urlToSelector.Id
            };

            var result = await new Handler(context).Handle(command, default(CancellationToken));

            var exists = context.UrlToSelector.Any(x => x.Id == urlToSelector.Id);
            Assert.True(result.Succeeded);
            Assert.False(exists);
        }
    }
}
