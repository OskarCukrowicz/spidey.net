﻿using Builders;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using SpideyWebApp.Features.RegexMatching.Edit;
using System.Threading;

namespace UnitTests.Features.RegexMatching
{

    public class EditTests
    {
        [Fact]
        public async Task ShouldEdit()
        {
            var context = new ContextBuilder().BuildClean();

            var urlToSelector = new UrlToSelectorBuilder(context)
                .WithRegularExpression("")
                .WithSelector("").BuildAndSave();

            var command = new Command()
            {
                Id = urlToSelector.Id,
                Regex = "[1-9]+",
                Selector = "asd"
            };

            var result = await new Handler(context).Handle(command, default(CancellationToken));

            Assert.True(result.Succeeded);
            Assert.Equal(command.Regex, urlToSelector.RegularExpression);
            Assert.Equal(command.Selector, urlToSelector.Selector);
        }
    }
}
