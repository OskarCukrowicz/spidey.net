﻿using Builders;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using SpideyWebApp.Features.RegexMatching.Add;
using System.Threading;

namespace UnitTests.Features.RegexMatching
{
    public class AddTests
    {
        [Fact]
        public async Task ShouldAddNewPair()
        {
            var context = new ContextBuilder().BuildClean();

            var command = new Command()
            {
                Regex = "[1-9]+",
                Selector = "body"
            };
            var result = await new Handler(context).Handle(command, default(CancellationToken));

            var urlToSelector = context.UrlToSelector.Find(result.Result.Id);

            Assert.True(result.Succeeded);
            Assert.NotNull(urlToSelector);
            Assert.Equal(command.Regex, urlToSelector.RegularExpression);
            Assert.Equal(command.Selector, urlToSelector.Selector);
        }
    }
}
