if __name__ == '__main__':
    import requests
    import json

    keywords = ['car']
    json_data = json.dumps({
                               'Body': 'A wagon (also alternatively and archaically spelt waggon in British and Commonwealth English) is a heavy four-wheeled vehicle pulled by draught animals or on occasion by humans (see below), used for transporting goods, commodities, agricultural materials, supplies and sometimes people.',
                               'TextInHeaders': '', 'Url': 'Wagon', 'Keywords': keywords,
                               'RedisKey': 'spidey.net'})
    requests.post('http://127.0.0.1:9875/evaluate', json_data)
    json_data = json.dumps({
                               'Body': 'A car (or automobile) is a wheeled motor vehicle used for transportation. Most definitions of car say they run primarily on roads, seat one to eight people, have four tires, and mainly transport people rather than goods.',
                               'TextInHeaders': '', 'Url': 'car', 'Keywords': keywords,
                               'RedisKey': 'spidey.net'})
    requests.post('http://127.0.0.1:9875/evaluate', json_data)
    json_data = json.dumps({
                               'Body': 'A bicycle, also called a cycle or bike, is a human-powered or motor-powered, pedal-driven, single-track vehicle, having two wheels attached to a frame, one behind the other. A bicycle rider is called a cyclist, or bicyclist.',
                               'TextInHeaders': '', 'Url': 'bicycle', 'Keywords': keywords, 'RedisKey': 'spidey.net'})
    requests.post('http://127.0.0.1:9875/evaluate', json_data)

    import redis
    import os

    data = {"RedisKey": 'spidey.net'}
    r = redis.Redis(host=os.environ['redis_url'], port=os.environ['redis_port'], db=0)
    for e in r.lrange(data['RedisKey'], 0, -1):
        print(e)
