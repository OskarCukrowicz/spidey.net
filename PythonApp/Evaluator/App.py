from flask import Flask
import json
import numpy as np
import string
from gensim import downloader
import multiprocessing
import flask

app = Flask(__name__)

wage_exponent = 5
header_multiplier = 4


def remove_punctuation(s):
    table = s.maketrans({key: None for key in string.punctuation})
    return s.translate(table)


def very_short_test_data_set():
    return ['very', 'short', 'test', 'data', 'set']


model = downloader.load("glove-wiki-gigaword-100")

thread_pool = multiprocessing.pool.ThreadPool(processes=4)


def evaluate_basing_on_most_similar(data):
    result = 0
    for keyword in data['Keywords']:
        single_keyword_value = data['Body'].count(keyword)
        single_keyword_value += data['TextInHeaders'].count(keyword) * header_multiplier
        try:
            similar = model.most_similar(keyword)
            for e in similar:
                single_keyword_value += data['Body'].count(e[0]) * np.power(e[1], wage_exponent)
                single_keyword_value += data['TextInHeaders'].count(e[0]) * np.power(e[1], wage_exponent)
            result += single_keyword_value
        except:
            result += single_keyword_value
    return result / (len(data['Body']) + header_multiplier * len(data['TextInHeaders']))


def sum_cosine_similarities(keywords, evaluated):
    result = 0.0
    for keyword in keywords:
        for word in evaluated:
            try:
                result += np.power(model.similarity(keyword, word), wage_exponent)
            except KeyError:
                pass
    return result


def evaluate_basing_on_every_words_cosine_similarity(data):
    result = sum_cosine_similarities(data['Keywords'], data['Body'])
    result += sum_cosine_similarities(data['Keywords'], data['TextInHeaders']) * header_multiplier
    return result / (len(data['Body']) + header_multiplier * len(data['TextInHeaders']))


def deal_post_request(data):
    data['Body'] = remove_punctuation(data['Body']).split()
    data['TextInHeaders'] = remove_punctuation(data['TextInHeaders']).split()
    return evaluate_basing_on_every_words_cosine_similarity(data)


@app.route("/evaluate", methods=['POST'])
def post():
    data = json.loads(flask.request.data)
    result = deal_post_request(data)
    return json.dumps(result), 200


app.run(host="0.0.0.0", port=9876)
