Param([string] $imageTag)
 
if ([string]::IsNullOrEmpty($imageTag)) {
    $imageTag =  $(git rev-parse --abbrev-ref HEAD)
}

Write-Host "Building images with tag $imageTag" -ForegroundColor Yellow 

Push-Location -Path ../PythonApp/Evaluator
docker build -t spideynet/evaluator:$imageTag . 
Pop-Location